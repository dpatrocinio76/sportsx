USE [master]
GO

/****** Object:  Database [sportsxdb]    Script Date: 16/03/2021 00:52:37 ******/
CREATE DATABASE [sportsxdb]
    CONTAINMENT = NONE
    ON  PRIMARY
    ( NAME = N'sportsxdb', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\sportsxdb.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
    LOG ON
    ( NAME = N'sportsxdb_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\sportsxdb_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
    begin
        EXEC [sportsxdb].[dbo].[sp_fulltext_database] @action = 'enable'
    end
GO

ALTER DATABASE [sportsxdb] SET ANSI_NULL_DEFAULT OFF
GO

ALTER DATABASE [sportsxdb] SET ANSI_NULLS OFF
GO

ALTER DATABASE [sportsxdb] SET ANSI_PADDING OFF
GO

ALTER DATABASE [sportsxdb] SET ANSI_WARNINGS OFF
GO

ALTER DATABASE [sportsxdb] SET ARITHABORT OFF
GO

ALTER DATABASE [sportsxdb] SET AUTO_CLOSE OFF
GO

ALTER DATABASE [sportsxdb] SET AUTO_SHRINK OFF
GO

ALTER DATABASE [sportsxdb] SET AUTO_UPDATE_STATISTICS ON
GO

ALTER DATABASE [sportsxdb] SET CURSOR_CLOSE_ON_COMMIT OFF
GO

ALTER DATABASE [sportsxdb] SET CURSOR_DEFAULT  GLOBAL
GO

ALTER DATABASE [sportsxdb] SET CONCAT_NULL_YIELDS_NULL OFF
GO

ALTER DATABASE [sportsxdb] SET NUMERIC_ROUNDABORT OFF
GO

ALTER DATABASE [sportsxdb] SET QUOTED_IDENTIFIER OFF
GO

ALTER DATABASE [sportsxdb] SET RECURSIVE_TRIGGERS OFF
GO

ALTER DATABASE [sportsxdb] SET  DISABLE_BROKER
GO

ALTER DATABASE [sportsxdb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO

ALTER DATABASE [sportsxdb] SET DATE_CORRELATION_OPTIMIZATION OFF
GO

ALTER DATABASE [sportsxdb] SET TRUSTWORTHY OFF
GO

ALTER DATABASE [sportsxdb] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO

ALTER DATABASE [sportsxdb] SET PARAMETERIZATION SIMPLE
GO

ALTER DATABASE [sportsxdb] SET READ_COMMITTED_SNAPSHOT OFF
GO

ALTER DATABASE [sportsxdb] SET HONOR_BROKER_PRIORITY OFF
GO

ALTER DATABASE [sportsxdb] SET RECOVERY SIMPLE
GO

ALTER DATABASE [sportsxdb] SET  MULTI_USER
GO

ALTER DATABASE [sportsxdb] SET PAGE_VERIFY CHECKSUM
GO

ALTER DATABASE [sportsxdb] SET DB_CHAINING OFF
GO

ALTER DATABASE [sportsxdb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF )
GO

ALTER DATABASE [sportsxdb] SET TARGET_RECOVERY_TIME = 0 SECONDS
GO

ALTER DATABASE [sportsxdb] SET DELAYED_DURABILITY = DISABLED
GO

ALTER DATABASE [sportsxdb] SET  READ_WRITE
GO







USE [sportsxdb]
GO
ALTER AUTHORIZATION ON DATABASE::[sportsxdb] TO [sa]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tb_cliente](
                                   [id] [int] NOT NULL,
                                   [nomeClienteRazaoSocial] [varchar](100) NOT NULL,
                                   [tipoPessoa] [tinyint] NULL,
                                   [classificacao] [tinyint] NOT NULL,
                                   [cpfCnpj] [varchar](14) NULL,
                                   [cep] [varchar](10) NULL,
                                   [email] [varchar](100) NOT NULL,
                                   CONSTRAINT [PK_tb_cliente] PRIMARY KEY CLUSTERED
                                       (
                                        [id] ASC
                                           )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE TABLE [dbo].[tb_telefone](
                                    [id] [int] NOT NULL,
                                    [numero] [varchar](12) NULL,
                                    [id_cliente] [int] NOT NULL,
                                    CONSTRAINT [PK_tb_telefone] PRIMARY KEY CLUSTERED
                                        (
                                         [id] ASC
                                            )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tb_telefone]  WITH CHECK ADD  CONSTRAINT [FK_tb_cliente_tb_telefone] FOREIGN KEY([id_cliente])
    REFERENCES [dbo].[tb_cliente] ([id])
    ON UPDATE CASCADE
    ON DELETE CASCADE
GO

ALTER TABLE [dbo].[tb_telefone] CHECK CONSTRAINT [FK_tb_cliente_tb_telefone]
GO
