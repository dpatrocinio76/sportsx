package br.com.vtt.sportsx.enums;

public enum ClassificacaoEnum {
    ATIVO("Ativo"),
    INATIVO("Inativo"),
    PREFERENCIAL("Preferencial");

    private String classificacaoEnum;

    ClassificacaoEnum(String classificacaoEnum) {
        this.classificacaoEnum = classificacaoEnum;
    }

    static public ClassificacaoEnum getEClassificacaoEnum(Integer ordinalClassificacaoEnum) {
        ClassificacaoEnum retorno = null;
        for (ClassificacaoEnum classificacaoEnum :ClassificacaoEnum.values()) {
            if (Integer.valueOf(classificacaoEnum.ordinal()).equals(ordinalClassificacaoEnum)) {
                return classificacaoEnum;
            }
        }
        return null;
    }
}
