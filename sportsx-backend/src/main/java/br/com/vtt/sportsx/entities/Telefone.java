package br.com.vtt.sportsx.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tb_telefone")
public class Telefone implements Serializable {
	
	private static final long serialVersionUID = 6524560251526772839L;

	private Long id;
	private String numero;

	private Cliente cliente;

	public Telefone() {
	}

	public Telefone(String numero, Cliente cliente) {
		this.numero = numero;
		this.cliente = cliente;
	}

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "numero", nullable = false)
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero= numero;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
}
