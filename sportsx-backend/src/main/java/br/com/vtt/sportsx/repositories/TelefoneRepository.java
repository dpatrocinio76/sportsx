package br.com.vtt.sportsx.repositories;

import br.com.vtt.sportsx.entities.Telefone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface TelefoneRepository extends JpaRepository<Telefone, Long> {

    @Modifying
    @Query("DELETE Telefone t WHERE t.cliente.id = ?1")
    void deleteByClienteId(Long clienteId);

}
