package br.com.vtt.sportsx;

import br.com.vtt.sportsx.entities.Cliente;
import br.com.vtt.sportsx.entities.Telefone;
import br.com.vtt.sportsx.enums.ClassificacaoEnum;
import br.com.vtt.sportsx.enums.TipoPessoaEnum;
import br.com.vtt.sportsx.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@EnableCaching
public class SportsxApplication {

	@Autowired
	private ClienteRepository clienteRepository;

	public static void main(String[] args) {
		SpringApplication.run(SportsxApplication.class, args);
	}

	@Component
	public class CommandLineAppStartupRunner implements CommandLineRunner {

		@Override
		public void run(String...args) throws Exception {
//			gerarDadosIniciaisClientes();
		}
	}

	private void gerarDadosIniciaisClientes() {


		Cliente cliente = new Cliente();
		cliente.setNomeClienteRazaoSocial("Cliente 1");
		cliente.setCep("30000000");
		cliente.setClassificacao(ClassificacaoEnum.ATIVO);
		cliente.setTipoPessoaEnum(TipoPessoaEnum.FISICA);
		cliente.setCpfCnpj("99999999999");
		cliente.setEmail("cliente1@vtt.com");

		List<Telefone> telefones = new ArrayList<>();
		Telefone telefone = new Telefone();
		telefone.setNumero("3199991234");
		Telefone telefone2 = new Telefone();
		telefone2.setNumero("3133334444");

		telefone.setCliente(cliente);
		telefone2.setCliente(cliente);
		telefones.add(telefone);
		telefones.add(telefone2);

		cliente.setTelefones(telefones);

		clienteRepository.save(cliente);

//-------------------------------------------------------------------

		Cliente cliente2 = new Cliente();
		cliente2.setNomeClienteRazaoSocial("Cliente 2");
		cliente2.setCep("32000002");
		cliente2.setClassificacao(ClassificacaoEnum.PREFERENCIAL);
		cliente2.setTipoPessoaEnum(TipoPessoaEnum.JURIDICA);
		cliente2.setCpfCnpj("39534089000123");
		cliente2.setEmail("cliente2@vtt.com");

		List<Telefone> telefones2 = new ArrayList<>();
		Telefone telefone3 = new Telefone();
		telefone3.setNumero("7199990000");
		Telefone telefone4 = new Telefone();
		telefone4.setNumero("7133330001");

		telefone3.setCliente(cliente2);
		telefone4.setCliente(cliente2);
		telefones2.add(telefone3);
		telefones2.add(telefone4);

		cliente2.setTelefones(telefones2);

		clienteRepository.save(cliente2);
		
	}

}
