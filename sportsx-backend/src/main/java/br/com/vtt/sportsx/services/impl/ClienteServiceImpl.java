package br.com.vtt.sportsx.services.impl;

import br.com.vtt.sportsx.entities.Cliente;
import br.com.vtt.sportsx.repositories.ClienteRepository;
import br.com.vtt.sportsx.services.ClienteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteServiceImpl implements ClienteService {

	private static final Logger log = LoggerFactory.getLogger(ClienteServiceImpl.class);

	@Autowired
	private ClienteRepository clienteRepository;

	public Page<Cliente> buscarTodosClientes(PageRequest pageRequest) {
		log.info("Buscando todos os clientes");
		return this.clienteRepository.findAll(pageRequest);
	}

	@Override
	public Page<Cliente> pesquisaPeloNomeClienteRazaoSocial(String nomeClienteRazaoSocial, PageRequest pageRequest) {
		return clienteRepository.findByNomeClienteRazaoSocialIgnoreCaseContaining(nomeClienteRazaoSocial, pageRequest);
	}

	@Cacheable("clientePorId")
	public Optional<Cliente> buscarClientePorId(Long id) {
		log.info("Buscando um cliente pelo ID {}", id);
		return Optional.ofNullable(this.clienteRepository.findOne(id));
	}
	
	@CachePut("clientePorId")
	public Cliente persistirCliente(Cliente cliente) {
		log.info("Persistindo cliente: {}", cliente);
		return this.clienteRepository.save(cliente);
	}

    public void remover(Long id) {
        log.info("Removendo o cliente ID {}", id);
        this.clienteRepository.delete(id);
    }

}
