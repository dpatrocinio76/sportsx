package br.com.vtt.sportsx.dtos;

import br.com.vtt.sportsx.entities.Telefone;

import java.io.Serializable;

public class TelefoneDto implements Serializable {

    private Long id;
    private String[] telefones;


	public TelefoneDto() {
	}

    public TelefoneDto(Telefone telefone) {
	    this.id = telefone.getId();

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
