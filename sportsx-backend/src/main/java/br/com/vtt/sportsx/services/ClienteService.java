package br.com.vtt.sportsx.services;

import br.com.vtt.sportsx.entities.Cliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface ClienteService {

	
	/**
	 * Retorna um cliente por ID.
	 * 
	 * @param id do cliente
	 * @return Optional<Cliente>
	 */
	Optional<Cliente> buscarClientePorId(Long id);
	
	/**
	 * Persiste um cliente na base de dados.
	 * 
	 * @param cliente a ser persistido na base de dados
	 * @return {@link Cliente} persistido na base de dados
	 */
	Cliente persistirCliente(Cliente cliente);

	/**
	 * Retorna uma lista com todos os clientes com paginacao
	 * @param pageRequest 
	 *
	 * @return List<Cliente> com todos os clientes com paginacao
	 */
    Page<Cliente> buscarTodosClientes(PageRequest pageRequest);

	/**
	 * Retorna uma lista dos clientes filtrados em pesquisa com paginacao
	 * @param pageRequest
	 *
	 * @return List<Cliente> com clientes filtrados em pesquisa com paginacao
	 */
	Page<Cliente> pesquisaPeloNomeClienteRazaoSocial(String nomeClienteRazaoSocial, PageRequest pageRequest);
    
	/**
	 * Remove um cliente da base de dados.
	 *
	 * @param id
	 */
	void remover(Long id);

}
