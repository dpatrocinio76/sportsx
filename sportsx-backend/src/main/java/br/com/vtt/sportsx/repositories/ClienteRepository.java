package br.com.vtt.sportsx.repositories;

import br.com.vtt.sportsx.entities.Cliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface ClienteRepository extends JpaRepository<Cliente, Long> {

    Page<Cliente> findByNomeClienteRazaoSocialIgnoreCaseContaining(String nomeClienteRazaoSocial, Pageable pageable);
}
