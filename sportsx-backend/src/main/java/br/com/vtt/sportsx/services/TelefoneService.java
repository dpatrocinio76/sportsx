package br.com.vtt.sportsx.services;

public interface TelefoneService {

	/**
	 * Remove todos os telefones de um cliente
	 *
	 * @param idCliente cliente
	 */
	void removerTodosTelefonesCliente(Long idCliente);

}
