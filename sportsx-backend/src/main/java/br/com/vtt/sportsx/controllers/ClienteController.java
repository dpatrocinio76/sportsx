package br.com.vtt.sportsx.controllers;

import br.com.vtt.sportsx.dtos.ClienteDto;
import br.com.vtt.sportsx.entities.Cliente;
import br.com.vtt.sportsx.entities.Telefone;
import br.com.vtt.sportsx.enums.ClassificacaoEnum;
import br.com.vtt.sportsx.enums.TipoPessoaEnum;
import br.com.vtt.sportsx.response.Response;
import br.com.vtt.sportsx.services.ClienteService;
import br.com.vtt.sportsx.services.TelefoneService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/cliente")
@CrossOrigin(origins = "*")
public class ClienteController {

	private static final Logger log = LoggerFactory.getLogger(ClienteController.class);

	@Autowired
	private ClienteService clienteService;

	@Autowired
	private TelefoneService telefoneService;

	@Value("${paginacao.qtd_por_pagina}")
	private int qtdPorPagina;

	public ClienteController() {
	}

	/**
	 * Endpoint para adicionar um novo cliente
	 * @param clienteDto parametro @{@link ClienteDto} para adicionar um novo cliente
	 * @param result controle do status da requisição
	 * @return retorna o @{@link ClienteDto} com dados do cliente adicionado
	 */
	@PostMapping
	public ResponseEntity<Response<ClienteDto>> adicionarCliente(@Valid @RequestBody ClienteDto clienteDto, BindingResult result) {

		log.info("Adicionando cliente: {}", clienteDto.toString());
		Response<ClienteDto> response = new Response<>();

		String mensagemInvalidacao = validarCliente(clienteDto);
		if (mensagemInvalidacao != null) {
			log.info("Erro ao adicionar cliente: {}", mensagemInvalidacao);
			response.getErrors().add(mensagemInvalidacao);
			return ResponseEntity.badRequest().body(response);
		}

		Cliente cliente = new Cliente(clienteDto);
		atualizarTelefonesCliente(clienteDto, cliente);

		cliente = this.clienteService.persistirCliente(cliente);
		response.setData(new ClienteDto(cliente));
		return ResponseEntity.ok(response);
    }

	/**
	 * Atualiza um cliente cadastrado
	 * @param idCliente identificador do cliente a ser alterado
	 * @param clienteDto dados para atualização
	 * @param result controle do status da requisição
	 * @return @{@link ClienteDto} com os dados do cliente atualizado
	 */
    @PutMapping(value = "/{id}")
    public ResponseEntity<Response<ClienteDto>> atualizar(@PathVariable("id") Long idCliente,
                                                             @Valid @RequestBody ClienteDto clienteDto, BindingResult result) {
        log.info("Atualizando cliente: {}", clienteDto.toString());
        Response<ClienteDto> response = new Response<>();

		String mensagemInvalidacao = validarCliente(clienteDto);
		if (mensagemInvalidacao != null) {
			log.info("Erro ao atualizar cliente: {}", mensagemInvalidacao);
			response.getErrors().add(mensagemInvalidacao);
			return ResponseEntity.badRequest().body(response);
		}

		Optional<Cliente> cliente = clienteService.buscarClientePorId(idCliente);
		cliente.get().setNomeClienteRazaoSocial(clienteDto.getNomeClienteRazaoSocial());
		cliente.get().setTipoPessoaEnum(TipoPessoaEnum.getTipoPessoaEnum(clienteDto.getTipoPessoaEnum()));
		cliente.get().setClassificacao(ClassificacaoEnum.getEClassificacaoEnum(clienteDto.getClassificacao()));
		cliente.get().setCpfCnpj(clienteDto.getCpfCnpj());
		cliente.get().setCep(clienteDto.getCep());
		cliente.get().setEmail(clienteDto.getEmail());

		telefoneService.removerTodosTelefonesCliente(idCliente);
		atualizarTelefonesCliente(clienteDto, cliente.get());

		clienteService.persistirCliente(cliente.get());

		response.setData(new ClienteDto(cliente.get()));
		return ResponseEntity.ok(response);
    }

	/**
	 * Remove um cliente existente
	 * @param id identificador do cliente a ser removido
	 * @return
	 */
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Response<String>> remover(@PathVariable("id") Long id) {
		log.info("Removendo cliente: {}", id);
		Response<String> response = new Response<String>();
		Optional<Cliente> cliente = this.clienteService.buscarClientePorId(id);

		if (!cliente.isPresent()) {
			log.info("Erro ao remover devido ao cliente ID: {} ser inválido.", id);
			response.getErrors().add("Erro ao remover cliente. Registro não encontrado para o id " + id);
			return ResponseEntity.badRequest().body(response);
		}

		this.clienteService.remover(id);
		return ResponseEntity.ok(new Response<>());
	}

	/**
	 * Pesquisa todos os clientes com recurso de paginação
	 * @param pag offset da paginação
	 * @param ord campo a ser ordenado na pesquisa
	 * @param dir direção da ordenação na pesquisa
	 * @return lista dos @{@link ClienteDto} com dados dos clientes pesquisados
	 */
	@GetMapping(value = "/consultar/todos/paginacao")
	public ResponseEntity<Response<Page<ClienteDto>>> pesquisarClientesPaginacao(@RequestParam(value = "pag", defaultValue = "0") int pag,
																			   @RequestParam(value = "ord", defaultValue = "id") String ord,
																			   @RequestParam(value = "dir", defaultValue = "DESC") String dir) {
		
		log.info("Buscando todos os clientes");
		Response<Page<ClienteDto>> response = new Response<Page<ClienteDto>>();
		PageRequest pageRequest = new PageRequest(pag, this.qtdPorPagina, Direction.valueOf(dir), ord);
		Page<Cliente> clientesPaginacao = clienteService.buscarTodosClientes(pageRequest);
		Page<ClienteDto> clientesDto = clientesPaginacao.map(c -> new ClienteDto(c));
		response.setData(clientesDto);
		
		return ResponseEntity.ok(response);
	}

	/**
	 * Pesquisa clientes pelo filtro do nome com recurso de paginação. O filtro busca pelo nome do cliente parcialmente
	 * @param pag offset da paginação
	 * @param ord campo a ser ordenado na pesquisa
	 * @param dir direção da ordenação na pesquisa
	 * @param filtroNomeClienteRazaoSocial filtro para pesquisa parcial do nome do cliente
	 * @return lista dos @{@link ClienteDto} com dados dos clientes pesquisados pelo filtro parcial
	 */
	@GetMapping(value = "/consultar/filtro/paginacao")
	public ResponseEntity<Response<Page<ClienteDto>>> pesquisarClientesFiltroPaginacao(@RequestParam(value = "pag", defaultValue = "0") int pag,
																				 @RequestParam(value = "ord", defaultValue = "id") String ord,
																				 @RequestParam(value = "dir", defaultValue = "DESC") String dir,
																					   @RequestParam(value = "filtroNomeClienteRazaoSocial", defaultValue = "") String filtroNomeClienteRazaoSocial) {

		log.info("Buscando clientes por filtro");
		Response<Page<ClienteDto>> response = new Response<Page<ClienteDto>>();
		PageRequest pageRequest = new PageRequest(pag, this.qtdPorPagina, Direction.valueOf(dir), ord);

		if (!"".equals(filtroNomeClienteRazaoSocial)) {
			Page<Cliente> clientesPaginacao = clienteService.pesquisaPeloNomeClienteRazaoSocial(filtroNomeClienteRazaoSocial, pageRequest);
			Page<ClienteDto> clientesDto = clientesPaginacao.map(c -> new ClienteDto(c));
			response.setData(clientesDto);
		} else {
			log.info("A pesquisa de clientes está com o filtro vazio");
			response.getErrors().add("A pesquisa de clientes está com o filtro vazio");
			return ResponseEntity.badRequest().body(response);
		}

		return ResponseEntity.ok(response);
	}

	/**
	 * Consulta um cliente pelo seu identificador
	 * @param idCliente identificador do cliente
	 * @return @{@link ClienteDto} com os dados do cliente com identificador passado por parametro
	 */
	@GetMapping(value = "/consultar/{idCliente}")
	public ResponseEntity<Response<ClienteDto>> consultarClientePorId(@PathVariable("idCliente") Long idCliente) {
		log.info("Buscando cliente por ID: {}", idCliente);
		Response<ClienteDto> response = new Response<ClienteDto>();
		Optional<Cliente> cliente = this.clienteService.buscarClientePorId(idCliente);

		if (!cliente.isPresent()) {
			log.info("Cliente não encontrado para o ID: {}", idCliente);
			response.getErrors().add("Cliente não encontrado para o id " + idCliente);
			return ResponseEntity.badRequest().body(response);
		} else {
            response.setData(new ClienteDto(cliente.get()));
        }
		return ResponseEntity.ok(response);
	}

	/**
	 * Atualiza dados dos telefones do Cliente
	 * @param clienteDto telefones a serem atualizados
	 * @param cliente cliente para atualização dos telefones
	 */
	private void atualizarTelefonesCliente(@RequestBody @Valid ClienteDto clienteDto, Cliente cliente) {
		List<Telefone> telefones = new ArrayList<>();
		if (clienteDto.getTelefones() != null && !clienteDto.getTelefones().isEmpty()) {
			clienteDto.getTelefones().forEach(t -> {
				if (t != null && !"".equals(t.trim())) {
					t = t.replace("(","").replace(")","").replace("-","");
					telefones.add(new Telefone(t,cliente));
			} });
		}
		cliente.setTelefones(telefones);
	}

	/**
	 * Validação do @{@link ClienteDto} com dados recebidos por parametro para serem persistidos no @{@link Cliente}
	 * @param clienteDto contem dados para persistir
	 * @return mensagem de erro caso esteja inválido ou nulo caso o @{@link ClienteDto} esteja válido
	 */
    private String validarCliente(ClienteDto clienteDto) {

		if (clienteDto.getNomeClienteRazaoSocial() == null) {
			return "O nome do cliente ou razão social não pode ser nulo";
		} else if ("".equals(clienteDto.getNomeClienteRazaoSocial().trim())) {
			return "O nome do cliente ou razão social não pode ser vazio";
		} else if (clienteDto.getNomeClienteRazaoSocial().length() > 100) {
			return "O nome do cliente ou razão social não ter mais que 100 caracteres";
		}

		if (clienteDto.getEmail() == null) {
			return "O e-mail não pode ser nulo";
		} else if ("".equals(clienteDto.getEmail().trim())) {
			return "O email não pode ser vazio";
		}

		if (clienteDto.getClassificacao() == null) {
			return "A classificação não pode ser nula";
		}

		return null;
    }


}
