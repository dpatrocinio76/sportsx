package br.com.vtt.sportsx.enums;

public enum TipoPessoaEnum {
    FISICA("Pessoa Física"),
    JURIDICA("Pessoa Jurídica");

    private String tipoPessoaEnum;

    TipoPessoaEnum(String tipoPessoaEnum) {
        this.tipoPessoaEnum = tipoPessoaEnum;
    }

    public String getTipoPessoaEnum() {
        return tipoPessoaEnum;
    }

    static public TipoPessoaEnum getTipoPessoaEnum(Integer ordinalTipoPessoa) {
        for (TipoPessoaEnum tipoPessoa1Enum : TipoPessoaEnum.values()) {
            if (Integer.valueOf(tipoPessoa1Enum.ordinal()).equals(ordinalTipoPessoa)) {
                return tipoPessoa1Enum;
            }
        }
        return null;
    }

}
