package br.com.vtt.sportsx.services.impl;

import br.com.vtt.sportsx.repositories.TelefoneRepository;
import br.com.vtt.sportsx.services.TelefoneService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TelefoneServiceImpl implements TelefoneService {

	private static final Logger log = LoggerFactory.getLogger(TelefoneServiceImpl.class);

	@Autowired
	private TelefoneRepository telefoneRepository;

	@Override
	public void removerTodosTelefonesCliente(Long idCliente) {
		log.info("Removendo todos os telefones do cliente de ID {}", idCliente);
		this.telefoneRepository.deleteByClienteId(idCliente);
	}
}
