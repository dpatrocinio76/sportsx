package br.com.vtt.sportsx.entities;

import br.com.vtt.sportsx.dtos.ClienteDto;
import br.com.vtt.sportsx.enums.ClassificacaoEnum;
import br.com.vtt.sportsx.enums.TipoPessoaEnum;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "tb_cliente")
public class Cliente implements Serializable {
	
	private static final long serialVersionUID = 6524560251526772839L;

	private Long id;
	private String nomeClienteRazaoSocial;
	private TipoPessoaEnum tipoPessoaEnum;
	private ClassificacaoEnum classificacao;
	private String cpfCnpj;
	private String cep;
	private String email;
	private List<Telefone> telefones;
	
	public Cliente() {
	}

	public Cliente(Long id, String nomeClienteRazaoSocial, TipoPessoaEnum tipoPessoaEnum, String cpfCnpj, String cep, ClassificacaoEnum classificacao, String email, List<Telefone> telefones) {
		this.id = id;
		this.nomeClienteRazaoSocial = nomeClienteRazaoSocial;
		this.tipoPessoaEnum = tipoPessoaEnum;
		this.cpfCnpj = cpfCnpj;
		this.cep = cep;
		this.classificacao = classificacao;
		this.email = email;
		this.telefones = telefones;
	}

	public Cliente(ClienteDto clienteDto) {
		this.id = clienteDto.getId();
		this.nomeClienteRazaoSocial = clienteDto.getNomeClienteRazaoSocial();
		this.tipoPessoaEnum = TipoPessoaEnum.getTipoPessoaEnum(clienteDto.getTipoPessoaEnum());
		this.cpfCnpj = clienteDto.getCpfCnpj();
		this.cep = clienteDto.getCep();
		this.email = clienteDto.getEmail();
		this.classificacao = ClassificacaoEnum.getEClassificacaoEnum(clienteDto.getClassificacao());
	}

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "nomeClienteRazaoSocial", nullable = false, length = 100)
	public String getNomeClienteRazaoSocial() {
		return this.nomeClienteRazaoSocial;
	}

	public void setNomeClienteRazaoSocial(String nomeClienteRazaoSocial) {
		this.nomeClienteRazaoSocial = nomeClienteRazaoSocial;
	}

	@Column(name = "cpfCnpj")
	public String getCpfCnpj() {
		return this.cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	@Column(name = "cep")
	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "classificacao", nullable = false)
	public ClassificacaoEnum getClassificacao() {
		return classificacao;
	}

	public void setClassificacao(ClassificacaoEnum classificacao) {
		this.classificacao = classificacao;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "tipoPessoa")
	public TipoPessoaEnum getTipoPessoaEnum() {
		return tipoPessoaEnum;
	}

	public void setTipoPessoaEnum(TipoPessoaEnum tipoPessoaEnum) {
		this.tipoPessoaEnum = tipoPessoaEnum;
	}

	@Column(name = "email", nullable = false)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@OneToMany(mappedBy = "cliente", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	public List<Telefone> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<Telefone> telefones) {
		this.telefones = telefones;
	}

	@Override
	public String toString() {
		return "Cliente{" +
				"id=" + id +
				", nomeClienteRazaoSocial='" + nomeClienteRazaoSocial + '\'' +
				", tipoPessoaEnum=" + tipoPessoaEnum +
				", classificacao=" + classificacao +
				", cpfCnpj='" + cpfCnpj + '\'' +
				", cep='" + cep + '\'' +
				", email='" + email + '\'' +
				", telefones=" + telefones +
				'}';
	}
}
