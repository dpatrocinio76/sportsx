package br.com.vtt.sportsx.dtos;

import br.com.vtt.sportsx.entities.Cliente;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ClienteDto implements Serializable {

    private Long id;
    private String nomeClienteRazaoSocial;
    private Integer tipoPessoaEnum;
    private String cpfCnpj;
    private String cep;
    private Integer classificacao;
    private String email;
    private List<String> telefones = new ArrayList<>();

	public ClienteDto() {
	}

    public ClienteDto(Cliente cliente) {
	    this.id = cliente.getId();
	    this.nomeClienteRazaoSocial = cliente.getNomeClienteRazaoSocial();
	    this.tipoPessoaEnum = cliente.getTipoPessoaEnum().ordinal();
	    this.cpfCnpj = cliente.getCpfCnpj();
	    this.cep = cliente.getCep();
	    this.classificacao = cliente.getClassificacao().ordinal();
        this.email = cliente.getEmail();

	    if (cliente.getTelefones() != null && !cliente.getTelefones().isEmpty())
            cliente.getTelefones().forEach(telefone -> this.telefones.add(telefone.getNumero()));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomeClienteRazaoSocial() {
        return nomeClienteRazaoSocial;
    }

    public void setNomeClienteRazaoSocial(String nomeClienteRazaoSocial) {
        this.nomeClienteRazaoSocial = nomeClienteRazaoSocial;
    }

    public Integer getTipoPessoaEnum() {
        return tipoPessoaEnum;
    }

    public void setTipoPessoaEnum(Integer tipoPessoaEnum) {
        this.tipoPessoaEnum = tipoPessoaEnum;
    }

    public String getCpfCnpj() {
        return cpfCnpj;
    }

    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public Integer getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(Integer classificacao) {
        this.classificacao = classificacao;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getTelefones() {
        return telefones;
    }

    public void setTelefones(List<String> telefones) {
        this.telefones = telefones;
    }
}
