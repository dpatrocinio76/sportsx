package br.com.vtt.sportsx.controllers;

import br.com.vtt.sportsx.entities.Cliente;
import br.com.vtt.sportsx.services.ClienteService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ClienteControllerTest {

	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private ClienteService clienteService;
	
	private static final String URL_BASE = "/api/cliente";
	
	@Test
	public void endpointPesquisarClientesPaginacaoTest() throws Exception {
		
		Page<Cliente> clientesPage = Mockito.mock(Page.class);
		BDDMockito.given(this.clienteService.buscarTodosClientes(Mockito.any(PageRequest.class))).willReturn(clientesPage);

		mvc.perform(MockMvcRequestBuilders.get(URL_BASE + "/consultar/todos/paginacao")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	public void endpointadicionarClienteTest() throws Exception {
		// TODO: implementar teste
		assertTrue(true);
	}

	@Test
	public void endpointatualizarTest() throws Exception {
		// TODO: implementar teste
		assertTrue(true);
	}

	@Test
	public void endpointremoverTest() throws Exception {
		// TODO: implementar teste
		assertTrue(true);
	}

	@Test
	public void endpointpesquisarClientesFiltroPaginacaoTest() throws Exception {
		// TODO: implementar teste
		assertTrue(true);
	}

	@Test
	public void endpointconsultarClientePorIdTest() throws Exception {
		// TODO: implementar teste
		assertTrue(true);
	}

}
