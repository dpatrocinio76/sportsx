package br.com.vtt.sportsx.repositories;

import br.com.vtt.sportsx.entities.Cliente;
import br.com.vtt.sportsx.entities.Telefone;
import br.com.vtt.sportsx.enums.ClassificacaoEnum;
import br.com.vtt.sportsx.enums.TipoPessoaEnum;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class TelefoneRepositoryTest {

	@Autowired
	private TelefoneRepository telefoneRepository;


	@Autowired
	private ClienteRepository clienteRepository;
	
	@Before
	public void setUp() throws Exception {
		Cliente pcliente = clienteRepository.save(criarCliente());
		assertNotNull(pcliente);
	}

	@After
	public void tearDown() throws Exception {
		this.clienteRepository.deleteAll();
	}

	@Test
	public void buscarTelefonesPorIdClienteTest() {

		telefoneRepository.deleteByClienteId(1L);
		List<Telefone> tels = telefoneRepository.findAll();
		assertNotNull(tels);
	}

	private Cliente criarCliente() {
		Cliente cliente = new Cliente();
		cliente.setNomeClienteRazaoSocial("Nome cliente ou Razao Social");
		cliente.setCpfCnpj("12345678900");
		cliente.setEmail("cliente@cliente.com");
		cliente.setCep("30123000");
		cliente.setClassificacao(ClassificacaoEnum.ATIVO);
		cliente.setTipoPessoaEnum(TipoPessoaEnum.FISICA);
		List<Telefone> telefones = new ArrayList<>();
		Telefone telefone = new Telefone();
		telefone.setNumero("31943211234");
		telefone.setCliente(cliente);
		telefones.add(telefone);
		cliente.setTelefones(telefones);
		return cliente;
	}

}
