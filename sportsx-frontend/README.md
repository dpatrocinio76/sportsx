# SportsX de Sistema artigos esportivos

### A API do sistema SportsX foi desenvolvido para atender os seguintes requisitos:

    1. O sistema deve permitir que o usuário faça cadastro de pessoa física e pessoa jurídica.
    2. Os campos Nome do Cliente (obrigatório) e Razão Social com tamanho de 100 caracteres.
    3. O campo CEP com máscara para digitação no formato 00-000-000.
    4. O campo E-mail (obrigatório) com validação de formato de e-mail.
    5. O campo Classificação (obrigatório) com as opções Ativo, Inativo, Preferencial.
    6. O sistema deve permitir o cadastro de 1 ou mais telefones para o mesmo cliente.
    7. Ao entrar no sistema seja possível visualizar os dados de todos os clientes ou que seja possível diferenciar pessoa física ou jurídica.
    8. Ao entrar no sistema seja possível na lista inicial ver os campos CPF/CNPJ (na mesma coluna), Nome do Cliente, Classificação e todos os telefones cadastrados.

### A arquitetura do sistema está definida da seguinte forma:

	Angular 5 e Angular Material: Framework Javascript para camada de frontend;
	Java 8: Linguagem de programação para backend;
	Spring Boot: Framework que facilita a construção de aplicações web e gestão de suas dependências; 
	Spring Web: Dependência do Spring Boot para criação de endpoints da API RESTfull e requisições HTTP;
	Web Service RESTfull: Exposição dos serviços web contendo as operações para atender os requisitos especificados; 
	Swagger: Suite de teste e documentação de endpoints da API;
	Spring Data: Framework que facilita a camada de persistência e recuperação de dados;  
	JPA/Hibernate: Framework para mapeamento objeto relacional e de persistencia;
	H2 (opcionalmente MySql): Banco de dados em memória. Opcionalmente pode-se usar o MySql disponível nas dependencias da aplicação;
	Flyway: Gerenciador de atualizações em bando de dados;
	EhCache: Otimizador de consultas em registros previamente cadastrados;
	JUnit/Mockito: Frameworks para implementação de testes automáticos;


### Como executar a aplicação banckend (Certifique-se de ter o Git instalado.)
```
git clone 
cd sportsx-backend
no windows: mvnw.cmd spring-boot:run
linux/macos: ./mvnw spring-boot:run 

API será executada em http://localhost:8080
```

### Para testar a API da aplicação sportsx-backend pelo browser, com a aplicação 'rodando', acessar a seguinte URL:
```
localhost:8080/swagger-ui.html
```

### Para executar a aplicação frontend:
```
git clone 
cd sportsx-frontend
npm install -g @angular/cli
npm install
ng serve
```


