import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {environment as env} from '../../../environments/environment';

import { Cliente } from '../models/';

@Injectable()
export class ClienteService {

  private readonly PATH: string = 'cliente';
  private readonly PATH_TODOS_CLIENTES_COM_PAGIACAO = '/consultar/todos/paginacao';
  private readonly PATH_FILTRO_CLIENTES_COM_PAGIACAO = '/consultar/filtro/paginacao';
  private readonly PATH_CONSULTAR_CLIENTE_PELO_ID = '/consultar/{idCliente}';
  private readonly PATH_CADASTRAR_CLIENTE = '/';


  constructor(private http: HttpClient) { }

  listarClientes(pagina: number, ordem: string, direcao: string): Observable<any> {

    const url: string = env.baseUrl + this.PATH + this.PATH_TODOS_CLIENTES_COM_PAGIACAO;
    const params: string = '?pag=' + pagina + '&ord=' + ordem + '&dir=' + direcao;
    return this.http.get(url + params);
  }

  pesquisarClientesPorFiltroPesquisa(pagina: number, ordem: string, direcao: string, filtroNomeClienteRazaoSocial: string): Observable<any> {

    const url: string = env.baseUrl + this.PATH + this.PATH_FILTRO_CLIENTES_COM_PAGIACAO;
    const params: string = '?pag=' + pagina + '&ord=' + ordem + '&dir=' + direcao + '&filtroNomeClienteRazaoSocial=' + filtroNomeClienteRazaoSocial;
    return this.http.get(url + params);
  }

  cadastrar(cliente: Cliente) {
    return this.http.post(
      env.baseUrl + this.PATH +  this.PATH_CADASTRAR_CLIENTE,
      cliente
    );
  }

  atualizar(cliente: Cliente): Observable<any> {
    return this.http.put(
      env.baseUrl + this.PATH + '/' + cliente.id,
      cliente
    );
  }

  remover(idCliente: number): Observable<any> {
    return this.http.delete(
      env.baseUrl + this.PATH + '/' + idCliente
    );
  }

  buscarPorId(idCliente: string): Observable<any> {
    return this.http.get(
        env.baseUrl + this.PATH + this.PATH_CONSULTAR_CLIENTE_PELO_ID.replace('{idCliente}', idCliente)
    );
  }
}










