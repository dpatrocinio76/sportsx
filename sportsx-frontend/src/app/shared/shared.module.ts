import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MascaraDirective} from './directives/mascara.directive';
import {CpfcnpjPipe} from './pipes';
import {TelefonePipe} from './pipes';
import {PtBrMatPaginatorIntl} from './';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
  	MascaraDirective,
    CpfcnpjPipe,
    TelefonePipe
  ],
  exports: [
  	MascaraDirective,
    CpfcnpjPipe,
    TelefonePipe
  ],
  providers: [
  	PtBrMatPaginatorIntl
  ]
})
export class SharedModule { }
