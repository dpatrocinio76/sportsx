
export class Cliente {

	constructor(
	  public nomeClienteRazaoSocial: string,
		public tipoPessoaEnum: number,
		public cpfCnpj: string,
		public cep: string,
    public classificacao: number,
    public email: string,
    public telefones: string[],
		public id?: string) {}
}
