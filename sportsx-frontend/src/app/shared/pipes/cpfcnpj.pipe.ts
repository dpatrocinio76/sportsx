import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'CEPCPFCNPJ'
})
export class CpfcnpjPipe implements PipeTransform {
  transform(value: string, ...args: any[]): any {
    if (value.length === 11) {
      return value.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, '\$1.\$2.\$3\-\$4');
    } else if (value.length === 14) {
      return value.replace(/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/, '\$1.\$2\.$3/$4-$5');
    } else if (value.length === 8) {
      return value.replace(/^(\d{2})(\d{3})(\d{3})/, '\$1.\$2-$3');
    }
    return '';
  }
}
