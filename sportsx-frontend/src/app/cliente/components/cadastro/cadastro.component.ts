import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs/Rx';

import {Cliente} from '../../../shared/models';
import {ClienteService} from '../../../shared/services';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit, OnDestroy {

  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private router: Router,
    private activeRouter: ActivatedRoute,
    private clienteService: ClienteService) {
  }

  idCliente: string;
  subscription: Subscription;
  editar:boolean = false;
  cliente:Cliente;

  ngOnInit() {
    this.subscription = this.activeRouter.params.subscribe((params: any) => {this.idCliente = params['id'];});
    this.editar = this.idCliente != '0';
    this.cliente = new Cliente(null,0,null,null,0, null,[],null);
    if (this.editar) {

      this.clienteService.buscarPorId(this.idCliente).subscribe(
        data => {
          this.cliente = data['data'];
        },
        err => {
          const msg: string = "Erro obtendo clientes.";
          this.snackBar.open(msg, "Erro", { duration: 5000 });
        }
      );
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  adicionarTelefone() {
    this.cliente.telefones.push("");
  }

  mudarValor(index, valor) {
    if (valor !== undefined && valor !== '' && valor.length === 13) {
      this.cliente.telefones[index] = valor;
    }
  }

  criarCliente() {

    if (this.validaCampos()) {

      if (this.editar) {
        this.clienteService.atualizar(this.cliente)
          .subscribe(
            data => {
              const msg: string = "Cliente Atualizado.";
              this.snackBar.open(msg, "Sucesso", { duration: 5000 });
              this.router.navigate(['/cliente']).then(() => { window.location.reload(); });
            },
            err => {
              let msg: string = "Erro ao atualizar cliente. Tente novamente mais tarde.";
              if (err.status == 400) {
                msg = err.error.errors.join(' ');
              }
              this.snackBar.open(msg, "Aviso de Erro", { duration: 5000 });
            }
          );

      } else {
        this.clienteService.cadastrar(this.cliente)
          .subscribe(
            data => {
              const msg: string = "Cliente Cadastrado.";
              this.snackBar.open(msg, "Sucesso", { duration: 5000 });
              this.router.navigate(['/cliente']).then(() => { window.location.reload(); });
            },
            err => {
              let msg: string = "\"Erro ao cadastrar cliente. Tente novamente mais tarde.";
              if (err.status == 400) {
                msg = err.error.errors.join(' ');
              }
              this.snackBar.open(msg, "Aviso de Erro", { duration: 5000 });
            }
          );
      }
    }
    return false;
  }

  validaCampos() {
    if (this.cliente.nomeClienteRazaoSocial == undefined || this.cliente.nomeClienteRazaoSocial === '') {
      this.snackBar.open("O campo Nome deve ser preenchido", "Aviso", { duration: 5000 });
      return false;
    } else if (this.cliente.nomeClienteRazaoSocial.length > 100) {
      this.snackBar.open("O campo Nome deve ter no máximo 100 caracteres", "Aviso", { duration: 5000 });
      return false;
    }
    if (this.cliente.email == undefined || this.cliente.email === '') {
      this.snackBar.open("O campo E-mail deve ser preenchido", "Aviso", { duration: 5000 });
      return false;
    }
    return true;
  }

  voltar() {
    this.router.navigate(['/cliente']).then(() => { window.location.reload(); });
  }

}
