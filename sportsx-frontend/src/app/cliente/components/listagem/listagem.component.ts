import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import { Router } from '@angular/router';
import {MAT_DIALOG_DATA, MatDialog, MatSelect, MatSnackBar, MatTableDataSource, PageEvent, Sort} from '@angular/material';

import {Cliente, ClienteService} from '../../../shared';

@Component({
  selector: 'app-listagem',
  templateUrl: './listagem.component.html',
  styleUrls: ['./listagem.component.css']
})
export class ListagemComponent implements OnInit {

  dataSource: MatTableDataSource<Cliente>;
  colunas: string[] = ['nomeClienteRazaoSocial', 'tipoPessoaEnum', 'cpfCnpj', 'email', 'cep', 'classificacao', 'acao'];
  totalContas: number;

  @ViewChild(MatSelect) matSelect: MatSelect;
  form: FormGroup;

  private pagina: number;
  private ordem: string;
  private direcao: string;

  private pesquisaValue: string;

  constructor(
  	private clienteService: ClienteService,
    private router: Router,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.pagina = 0;
    this.ordemPadrao();
    this.exibirClientes();
    this.gerarForm();
  }

  ordemPadrao() {
    this.ordem = 'id';
    this.direcao = 'DESC';
  }

  gerarForm() {
    this.form = this.fb.group({
      funcs: ['', []]
    });
  }

  cadastroCliente(){
    this.router.navigate(['cadastro',0]);
  }

  excluirCliente(idCliente:number) {
    this.clienteService.remover(idCliente)
      .subscribe(
        data => {
          const msg: string = "Cliente removido com sucesso!";
          this.snackBar.open(msg, "Sucesso", { duration: 5000 });
          this.exibirClientes();
        },
        err => {
          let msg: string = "Tente novamente em instantes.";
          if (err.status == 400) {
            msg = err.error.errors.join(' ');
          }
          this.snackBar.open(msg, "Erro", { duration: 5000 });
        }
      );
  }

  exibirClientes() {

    this.clienteService.listarClientes(this.pagina, this.ordem, this.direcao)
      .subscribe(
        data => {
          this.totalContas = data['data'].totalElements;
          const clientes = data['data'].content as Cliente[];
          this.dataSource = new MatTableDataSource<Cliente>(clientes);
          console.log(clientes);
        },
        err => {
          const msg: string = "Erro obtendo todos clientes.";
          this.snackBar.open(msg, "Erro", { duration: 5000 });
        }
      );
  }

  pesquisarPorFiltroPesquisa(){

    if (this.pesquisaValue == undefined || this.pesquisaValue === '') {
      this.exibirClientes();
    } else {
      this.clienteService.pesquisarClientesPorFiltroPesquisa(this.pagina, this.ordem, this.direcao, this.pesquisaValue)
        .subscribe(
          data => {
            this.totalContas = data['data'].totalElements;
            const clientes = data['data'].content as Cliente[];
            this.dataSource = new MatTableDataSource<Cliente>(clientes);
          },
          err => {
            const msg: string = "Erro obtendo clientes por filtro.";
            this.snackBar.open(msg, "Erro", { duration: 5000 });
          }
        );
    }
  }


  paginar(pageEvent: PageEvent) {
    this.pagina = pageEvent.pageIndex;
    this.exibirClientes();
  }

  ordenar(sort: Sort) {
    if (sort.direction == '') {
      this.ordemPadrao();
    } else {
      this.ordem = sort.active;
      this.direcao = sort.direction.toUpperCase();
    }
    this.exibirClientes();
  }

  visualizarTelefonesDialog(nomeClienteRazaoSocial:string, telefones: string[]) {
    console.log(nomeClienteRazaoSocial);
    console.log(telefones);
    console.log(this.dataSource);
    const dialog = this.dialog.open(TelefonesDialog, {data: {nomeClienteRazaoSocial:nomeClienteRazaoSocial, telefones: telefones}});
    dialog.afterClosed().subscribe(okay => { });
  }
}

@Component({
  selector: 'telefones-dialog',
  template: `
    <h1 mat-dialog-title>Telefones do cliente {{data.nomeClienteRazaoSocial}}</h1>
    <table>
      <tr *ngFor="let tel of data.telefones">
        <td>{{tel | TELEFONE }}</td>
      </tr>
    </table>
    <div mat-dialog-actions>
      <button mat-button [mat-dialog-close]="true" tabindex="2">
        OK
      </button>
    </div>
  `,
})

export class TelefonesDialog {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}
}
