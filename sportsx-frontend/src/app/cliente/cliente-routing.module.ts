import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
  ListagemComponent,
  ClienteComponent, CadastroComponent
} from './components';

export const ClienteRoutes: Routes = [
	{
		path: 'cliente',
		component: ClienteComponent,
		children: [
		  {
			path: '',
			component: ListagemComponent
		  }
		  // ,
      // {
      //   path: 'debito/:contaId',
      //   component: DebitoComponent
      // }
		]
	},
  {
    path: 'cadastro/:id',
    component: CadastroComponent
  }

];

@NgModule({
  imports: [
    RouterModule.forChild(ClienteRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ClienteRoutingModule {
}
